FROM google/debian:jessie

RUN apt-get update && \
    apt-get install -y curl build-essential

    # Installing node
RUN curl -sL https://deb.nodesource.com/setup_7.x | bash - && \
    apt-get install -y nodejs && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN npm i -g yarn

WORKDIR /src
ADD package.json yarn.lock ./
RUN yarn install

ADD . .

RUN ./node_modules/.bin/grunt build

EXPOSE 3006
ENV NODE_ENV production
CMD ["npm", "run", "server"]