var path = require("path");
var webpack = require("webpack");
var _ = require("lodash");

var buildOutput = path.join(__dirname, "build/static/scripts");

var baseConfig = {
  cache: true,
  entry: {
    app: ['./frontend/src/js/entries/client-entry.jsx']
  },
  output: {
    path: buildOutput,
    publicPath: "/static/scripts",
    filename: "[name].bundle.js",
    chunkFilename: "[chunkhash].js"
  },
  resolve: {
    extensions: ['', '.js', '.jsx', '.scss'],
    alias: {
      'front': path.join(__dirname, 'frontend/src/js'),
      'sass': path.join(__dirname, 'frontend/src/sass'),
      'server': path.join(__dirname, 'server/src'),
      'front-test': path.join(__dirname, 'frontend/test/js')
    }
  }
};

var serverConfig = {
  cache: true,
  entry: {
    app: [
      './frontend/src/js/entries/server-entry.jsx',
    ]
  },
  output: {
    path: buildOutput,
    filename: "server-frontend-bundle.js",
    libraryTarget: "commonjs2"
  },
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loaders: ['babel?compact=false'],
      },
      {
        test: /\.scss?$/,
        exclude: /node_modules/,
        loaders: ['style', 'css', 'sass']
      },
      {
        test: /\.json?$/,
        exclude: /node_modules/,
        loaders: ['json']
      }
    ]
  },
  resolve: {
    extensions: ['', '.js', '.jsx', '.scss'],
    alias: {
      'front': path.join(__dirname, 'frontend/src/js'),
      'sass': path.join(__dirname, 'frontend/src/sass'),
      'server': path.join(__dirname, 'server/src')
    }
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify("production"),
      'process.env.WEBPACK_ENV': JSON.stringify("server")
    })
  ]
};

//PROD CONFIG
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var prodConfig = _.extend({}, _.cloneDeep(baseConfig), {
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loaders: ['babel?compact=false']
      },
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        loader: ExtractTextPlugin.extract("style", "css?-autoprefixer&importLoaders=2!postcss!sass")
      },
      {
        test: /\.json?$/,
        exclude: /node_modules/,
        loaders: ['json']
      }
    ]
  },
  plugins: [
    new ExtractTextPlugin("styles.css"),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify("production"),
      'process.env.WEBPACK_ENV': JSON.stringify("client")
    }),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      }
    }),
    new webpack.optimize.DedupePlugin()
  ]
});

//DEV CONFIG
var devConfig = _.extend({}, _.cloneDeep(baseConfig), {
  devtool: "sourcemap",
  debug: true,
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loaders: ['babel?compact=false']
      },
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        loaders: ["style", "css?sourceMap", "postcss", "sass?sourceMap"]
      },
      {
        test: /\.json?$/,
        exclude: /node_modules/,
        loaders: ['json']
      }
    ]
  },
  plugins: [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development'),
      'process.env.WEBPACK_ENV': JSON.stringify("client")
    })
  ],
});
devConfig.entry.app.push(
  'webpack-hot-middleware/client'
);

module.exports = _.extend({}, baseConfig,
  {
    prodConfig: prodConfig,
    devConfigClient: devConfig,
    devConfigServer: serverConfig
  }
);
