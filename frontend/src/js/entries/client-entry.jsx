import 'react-hot-loader/patch';
import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';

import Routes from 'front/router';
import M from 'front/helpers/messages';

const messagesJSON = JSON.parse(document.getElementById("messages").innerHTML);
M.setMessages(messagesJSON.language, messagesJSON.messages);

ReactDOM.render(
<AppContainer>
  { Routes.setup() }
</AppContainer>, document.getElementById('main'));

if (module.hot) {
  module.hot.accept('front/router', () => {
    const RoutesContainer = require('front/router').default;
    ReactDOM.render(
      <AppContainer>
        { RoutesContainer.setup() }
      </AppContainer>, document.getElementById('main'));
  });
}

import Services from 'front/services';
window.Services = Services;

import math from 'mathjs';
window.math = math;
