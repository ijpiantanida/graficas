import React from 'react';
import {Router, Route, browserHistory} from 'react-router';
import App from 'front/components/app';
import HelloWorld from 'front/components/helloWorld';
import SignIn from 'front/components/sign-in/sign-in';
import Services from 'front/services';


export default {
  setup(req) {
    const requireSession = (nextState, replace) => {
      const validSession = Services.session.validSession(req);
      if (!validSession) {
        replace({
          pathname: '/sign-in',
          state: {nextPathname: nextState.location.pathname}
        });
      }
    };

    return (<Router history={browserHistory}>
      <Route path="/sign-in" component={SignIn}/>
      <Route path="/" component={App} onEnter={requireSession}>
        <Route path="/messages/:message" component={HelloWorld} onEnter={requireSession}/>
      </Route>
    </Router>);
  }
};
