let messages = {};
const registeredComponents = [];

const stateKey = "_MESSAGES_LANGUAGE";

export default {
  registeredComponents: registeredComponents,

  setMessages(language, newMessages) {
    this.selectedLanguage = language;
    messages = newMessages;
    const obj = {};
    obj[stateKey] = language;
    registeredComponents.forEach((c) => c.setState(obj));
  },

  registerComponent(component) {
    registeredComponents.push(component);
  },

  unregisterComponent(component) {
    const index = registeredComponents.indexOf(component);
    if (index > -1) {
      registeredComponents.splice(index, 1);
    }
  }
};

export function t(key, params, defaultValue) {
  let interpolatedValue = messages[key] || defaultValue || key;
  if (params) {
    for (let paramName of Object.keys(params)) {
      const value = params[paramName];
      interpolatedValue = interpolatedValue.replace(`{{${paramName}}}`, value);
    }
  }
  return interpolatedValue;
}
