export default {
  get(name, req) {
    if (process.env.WEBPACK_ENV === 'server') {
      return req.cookies[name];
    } else {
      const value = "; " + document.cookie;
      const parts = value.split("; " + name + "=");
      return parts.pop().split(";").shift();
    }
  },

  set(name, value) {
    document.cookie = name + "=" + value + "; path=/";
  }
};
