import React, {Component} from 'react';
import {Link} from 'react-router';
import M, {t} from 'front/helpers/messages';
import {browserHistory} from 'react-router';
import Services from 'front/services';

import ScheduleChart from 'front/components/schedule-chart';

export default class App extends Component {
  constructor(props, context) {
    super(props, context);
    this.signOut = this.signOut.bind(this);
    this.changeLanguage = this.changeLanguage.bind(this);
  }

  componentDidMount() {
    M.registerComponent(this);
    require('sass/app');
  }

  componentWillUnmount() {
    M.unregisterComponent(this);
  }

  signOut(e) {
    Services.session.signOut();
    browserHistory.push("/sign-in");
  }

  changeLanguage(language) {
    Services.language.setLanguage(language);
  }

  render() {
    let language = 'en';
    let languageDescription = 'EN';
    if (M.selectedLanguage === 'en') {
      language = 'es';
      languageDescription = 'ES';
    }

    return (
      <div>
        <div className="header">
          <h1>{t("app.name")}</h1>
          <button className="change-language" onClick={() => this.changeLanguage(language)}
                  data-bdd="navigation-language-es">{languageDescription}
          </button>
          <a onClick={this.signOut} data-bdd="navigation-sign-out">{t("navigation.sign_out")}</a>
        </div>
        <ul>
          <li><Link to="/messages/about">{t("navigation.about")}</Link></li>
          <li><Link to="/messages/inbox">{t("navigation.inbox")}</Link></li>
        </ul>
        { this.props.children }
        <ScheduleChart />
      </div>
    );
  }
}

App.propTypes = {
  children: React.PropTypes.object
};
