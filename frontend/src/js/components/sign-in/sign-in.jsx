import React, {Component} from 'react';
import {browserHistory} from 'react-router';
import Services from 'front/services';
import M, {t} from 'front/helpers/messages';

export default class SignIn extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      username: "",
      password: ""
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    M.registerComponent(this);
    require('sass/sign-in');
  }

  componentWillUnmount() {
    M.unregisterComponent(this);
  }

  onChange(field, e) {
    const obj = {};
    obj[field] = e.target.value;
    this.setState(obj);
  }

  onSubmit(e) {
    e.preventDefault();

    this.setState({failedAuth: false, loading: true});
    Services.session.authenticate(this.state.username, this.state.password)
      .then(() => {
        browserHistory.push('/');
      })
      .catch(() => {
        this.setState({failedAuth: true, loading: false});
      });
  }

  render() {
    let action = (<button type="submit">{t("sign-in.action")}</button>);
    if (this.state.loading) {
      action = (<span className="loading" data-bdd="loading-message">Signing in...</span>);
    }

    let errorMessage = undefined;
    if (this.state.failedAuth) {
      errorMessage = (<span className="error-message" data-bdd="error-message">Wrong username or password</span>);
    }

    return (
      <div>
        <h1>{t("app.name")}</h1>
        <div className="form-container">
          {errorMessage}
          <form onSubmit={this.onSubmit}>
            <input type="text" value={this.state.username} onChange={(e) => this.onChange('username', e)}
                   className="username" placeholder={t('sign-in.username')} data-bdd="username"/>
            <input type="password" value={this.state.password}
                   onChange={(e) => this.onChange('password', e)} className="password"
                   placeholder={t('sign-in.password')} data-bdd="password"/>
            {action}
          </form>
        </div>
      </div>
    );
  }
}
