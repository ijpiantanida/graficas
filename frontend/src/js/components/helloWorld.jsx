import React, {Component} from 'react';
import M, {t} from 'front/helpers/messages';

export default class HelloWorld extends Component {
  componentDidMount() {
    M.registerComponent(this);
    require('sass/helloWorld');
  }

  componentWillUnmount() {
    M.unregisterComponent(this);
  }

  render() {
    return (
      <div className="hello-world">{t("hello_world.message", {message: this.props.params.message})}</div>
    );
  }
}

HelloWorld.propTypes = {
  params: React.PropTypes.object
};
