import React, {Component} from 'react';

export default class HeaderTime extends Component {
  render() {
    const timeElements = [];
    this.props.chart.forEachTimeSlot((time) => {
      const [hours, mins] = time.splitUnit(['h', 'mins']);
      const hoursPadding = hours.toNumber() < 10 ? "0" : "";
      const minsPadding = mins.toNumber() < 10 ? "0" : "";
      timeElements.push((<div key={time} className="timetable-header-time" data-bdd="timetable-header-time">
        {hoursPadding + hours.toNumber().toString()}:{minsPadding + mins.toNumber().toString()}
      </div>));
    });
    return (<div className="timetable-column-header">
      {timeElements}
    </div>);
  }
};

HeaderTime.propTypes = {
  chart: React.PropTypes.object
};
