import React, {Component} from 'react';
import ScheduleChart from './';
import math from 'mathjs';
import Hammer from 'react-hammerjs';

export default class Assignment extends Component {
  constructor(state, props) {
    super(state, props);
    this.onDragStart = this.onDragStart.bind(this);
    this.onDrag = this.onDrag.bind(this);
    this.onDragEnd = this.onDragEnd.bind(this);
    this.state = {
      draggingLeft: false,
      draggingRight: false
    };
    this.scrollOffsetAtStart = 0;
  }

  onDragStart(handle, e) {
    if (handle === "start") {
      this.setState({draggingLeft: true, dragDifference: e.deltaX});
    } else {
      this.setState({draggingRight: true, dragDifference: e.deltaX});
    }
    this.scrollOffsetAtStart = this.props.scrollOffset;
  }

  onDrag(handle, e) {
    this.setState({dragDifference: e.deltaX || 0});
  }

  onDragEnd(handle, e) {
    const difference = e.deltaX + (this.props.scrollOffset - this.scrollOffsetAtStart);
    const stepsToAdjust = Math.round(difference / ScheduleChart.timeColumnWidth);

    const newAssignment = this.props.assignment.clone();
    if (handle === "start") {
      newAssignment.moveStartTimeBySteps(stepsToAdjust);
    } else {
      newAssignment.moveEndTimeBySteps(stepsToAdjust);
    }

    this.props.onAssignmentUpdate(newAssignment);

    this.setState({draggingLeft: false, draggingRight: false});
  }

  bounds() {
    const assignment = this.props.assignment;
    const chart = assignment.chart;

    const timeDifferenceWithOpenTime = math.subtract(assignment.startTime, chart.openTime);
    const duration = math.subtract(assignment.endTime, assignment.startTime);
    const numberOfStepsToStart = math.divide(timeDifferenceWithOpenTime, chart.step);
    const durationInSteps = math.divide(duration, chart.step);

    const maxChartWidth = math.divide(math.subtract(chart.closeTime, chart.openTime), chart.step) * ScheduleChart.timeColumnWidth;

    const originalLeft = numberOfStepsToStart * ScheduleChart.timeColumnWidth;
    let left = originalLeft;
    const originalDuration = durationInSteps * ScheduleChart.timeColumnWidth;
    let width = originalDuration;

    const difference = this.state.dragDifference + (this.props.scrollOffset - this.scrollOffsetAtStart);

    if (this.state.draggingLeft) {
      let expectedLeft = left + difference;
      left = expectedLeft;
      if (left < 0) {
        left = 0;
      }

      width -= difference - (expectedLeft - left);

      if (width < ScheduleChart.timeColumnWidth) {
        width = ScheduleChart.timeColumnWidth;
        left = originalLeft + originalDuration - width;
      }
    }
    if (this.state.draggingRight) {
      width += difference;

      if (width < ScheduleChart.timeColumnWidth) {
        width = ScheduleChart.timeColumnWidth;
      }

      if (left + width > maxChartWidth) {
        width = maxChartWidth - left;
      }
    }

    return {left, width};
  }

  render() {
    const bounds = this.bounds();

    return (<div className="timetable-assignment" style={bounds}>
      <Hammer onPanStart={(e) => this.onDragStart("start", e)}
              onPan={(e) => this.onDrag("start", e)} onPanEnd={(e) => this.onDragEnd("start", e)}>
        <div className="assignment-handle"/>
      </Hammer>
      <Hammer onPanStart={(e) => this.onDragStart("end", e)}
              onPan={(e) => this.onDrag("end", e)} onPanEnd={(e) => this.onDragEnd("end", e)}>
        <div className="assignment-handle"/>
      </Hammer>
    </div>);
  }
}

Assignment.propTypes = {
  assignment: React.PropTypes.shape({
    chart: React.PropTypes.object,
    startTime: React.PropTypes.object,
    endTime: React.PropTypes.object,
    clone: React.PropTypes.func
  }),
  onAssignmentUpdate: React.PropTypes.func,
  scrollOffset: React.PropTypes.number
};
