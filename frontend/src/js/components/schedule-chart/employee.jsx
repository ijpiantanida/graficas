import React, {Component} from 'react';

export default class Employee extends Component {
  render() {
    return (<div className="employees-column-row">
      {this.props.employee.name}
    </div>);
  }
}

Employee.propTypes = {
  employee: React.PropTypes.shape({
    name: React.PropTypes.string
  })
};
