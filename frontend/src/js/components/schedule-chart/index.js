import React, {Component} from 'react';
import Assignment from 'front/models/assignment';
import Chart from 'front/models/chart';
import _ from 'lodash';

import AssignmentComponent from './assignment';
import AssignmentSummary from './assignmentSummary';
import Employee from './employee';
import HeaderTime from './header-time';

export default class ScheduleChart extends Component {
  constructor(props, context) {
    super(props, context);
    this.onTimetableScroll = this.onTimetableScroll.bind(this);
    this.updateAssignment = this.updateAssignment.bind(this);
    this.addEmployee = this.addEmployee.bind(this);

    const chart = new Chart(8, 22, 0.5);
    chart.assignments = [
      new Assignment(chart, 1, 1, 9.5, 13),
      new Assignment(chart, 2, 2, 20, 20.5),
      new Assignment(chart, 3, 3, 8.5, 14.5),
      new Assignment(chart, 4, 2, 8.5, 18)
    ];
    chart.requiredAssignment = [0, 0, 3, 3, 4, 6, 8, 8, 3, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    this.state = {
      employees: [
        {id: 1, name: "Employee 1"},
        {id: 2, name: "Employee 2"},
        {id: 3, name: "Employee 3"}
      ],
      scrollOffset: 0,
      chart: chart
    };
  }

  componentDidMount() {
    require('sass/scheduler-chart');
  }

  addEmployee(e) {
    const newEmployees = this.state.employees.slice(0);
    const newEmployeeId = newEmployees.length + 1;
    newEmployees.push({
      id: newEmployeeId,
      name: "Employee " + newEmployeeId
    });

    const newAssignments = this.state.chart.assignments.slice(0);
    const newAssignmentId = newAssignments.length + 1;
    newAssignments.push(
      new Assignment(this.state.chart, newAssignmentId, newEmployeeId, 8, 9)
    );
    this.state.chart.assignments = newAssignments;

    this.setState({employees: newEmployees, chart: this.state.chart});
  }

  maxNumberOfRows() {
    const maxRequiredAssignment = _.max(this.state.chart.requiredAssignment);
    return _.max([this.state.employees.length, maxRequiredAssignment]);
  }

  addEmployeeButton(index) {
    return (<div key={index} className="employees-add-button" onClick={this.addEmployee}>+</div>);
  }

  employeesColumn() {
    const employeesList = [];
    const maxNumberOfRows = this.maxNumberOfRows();
    for (let i = 0; i < maxNumberOfRows; i++) {
      if (i < this.state.employees.length) {
        const e = this.state.employees[i];
        employeesList.push(<Employee key={i} employee={e}/>);
      } else {
        employeesList.push(this.addEmployeeButton(i));
      }
    }

    return (
      <div className="schedule-chart-employees">
        <span className="employees-column-header"/>
        { employeesList }
        { this.addEmployeeButton(maxNumberOfRows) }
      </div>
    );
  }

  onTimetableScroll(e) {
    this.setState({scrollOffset: e.target.scrollLeft});
  }

  timetableColumn() {
    return (
      <div className="schedule-chart-timetable" onScroll={this.onTimetableScroll}>
        <div className="schedule-chart-timetable-box">
          { <HeaderTime chart={this.state.chart}/> }
          { this.gridRows() }
          { <AssignmentSummary chart={this.state.chart}/> }
          { this.gridDividers() }
        </div>
      </div>
    );
  }

  gridDividers() {
    const dividers = [];
    this.state.chart.forEachTimeSlot((time) => {
      dividers.push(<div key={time} className="timetable-divider"/>);
    });
    dividers.pop();

    return (<div className="timetable-dividers">{ dividers }</div>);
  }

  updateAssignment(assignment) {
    const index = this.state.chart.assignments.findIndex((a) => a.id === assignment.id);

    this.state.chart.assignments = [
      ...this.state.chart.assignments.slice(0, index),
      assignment,
      ...this.state.chart.assignments.slice(index + 1)
    ];
    this.setState({chart: this.state.chart});
  }

  gridRows() {
    const rows = [];
    for (let i = 0; i < this.maxNumberOfRows(); i++) {
      const requiredAssignments = [];
      this.state.chart.forEachTimeSlot((time, timeSlotIndex) => {
        if (this.state.chart.requiredAssignment[timeSlotIndex] > i) {
          const left = timeSlotIndex * ScheduleChart.timeColumnWidth;
          requiredAssignments.push((<div key={i + "-" + timeSlotIndex}
                                         className="required-assignment"
                                         style={{left}}
          />));
        }
      });

      let assignmentElements = [];
      if (i < this.state.employees.length) {
        const e = this.state.employees[i];
        const assignments = this.state.chart.assignmentsForEmployee(e);
        assignmentElements = assignments.map(assignment => {
          return (<AssignmentComponent key={assignment.id}
                                      assignment={assignment}
                                      scrollOffset={this.state.scrollOffset}
                                      onAssignmentUpdate={this.updateAssignment}/>);
        });
      }

      rows.push(<div className="timetable-column-row" key={i}>
        { requiredAssignments }
        { assignmentElements }
      </div>);
    }
    return rows;
  }

  render() {
    return (
      <div className="schedule-chart">
        { this.employeesColumn() }
        { this.timetableColumn() }
      </div>
    );
  }
}
;

ScheduleChart.timeColumnWidth = 47;
