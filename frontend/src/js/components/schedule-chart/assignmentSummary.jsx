import React, {Component} from 'react';
import AssignmentSummaryModel from 'front/models/assignmentSummary';

export default class AssignmentSummary extends Component {
  render() {
    const summary = new AssignmentSummaryModel(this.props.chart);
    const summaryElements = [];
    summary.numberOfAssignmentsByTimeSlot().forEach((value, index) => {
      const valueToShow = value > 0 ? value : "";
      summaryElements.push(<div key={index} className="assignment-summary-entry" data-bdd="assignment-summary-entry">
        { valueToShow }
      </div>);
    });
    return (<div className="assignment-summary">
      { summaryElements }
    </div>);
  }
}

AssignmentSummary.propTypes = {
  chart: React.PropTypes.object.isRequired
};
