export default class AssignmentSummary {
  constructor(chart) {
    this.chart = chart;
  }

  numberOfAssignmentsByTimeSlot() {
    const counts = [];
    this.chart.forEachTimeSlot((time) => {
      const assignmentsInTimeSlot = this.chart.assignments.filter((a) => a.covers(time.toNumber()));
      counts.push(assignmentsInTimeSlot.length);
    });
    return counts;
  }
}
