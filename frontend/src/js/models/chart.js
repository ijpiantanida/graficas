import math from 'mathjs';

export default class Chart {
  constructor(openTime, closeTime, step) {
    this.openTime = math.unit(openTime, 'h');
    this.closeTime = math.unit(closeTime, 'h');
    this.step = math.unit(step, 'h');
    this.assignments = [];
  }

  forEachTimeSlot(callback) {
    const openTime = this.openTime;
    const closeTime = this.closeTime;
    const step = this.step;
    let index = 0;
    for (let now = openTime; !math.smaller(closeTime, now); now = math.add(now, step)) {
      callback(now, index);
      index += 1;
    }
  }

  assignmentsForEmployee(employee) {
    return this.assignments.filter((assignment) => assignment.employeeId === employee.id);
  }
}
