import math from 'mathjs';

export default class Assignment {
  constructor(chart, id, employeeId, startTime, endTime) {
    this.id = id;
    this.chart = chart;
    this.employeeId = employeeId;
    this.startTime = math.unit(startTime, 'h');
    this.endTime = math.unit(endTime, 'h');
  }

  moveStartTimeBySteps(steps) {
    let newStartTime = math.add(this.startTime, math.multiply(steps, this.chart.step));

    if (!math.smaller(this.chart.openTime, newStartTime)) {
      newStartTime = this.chart.openTime;
    }
    if (!math.smaller(newStartTime, this.endTime)) {
      newStartTime = math.subtract(this.endTime, this.chart.step);
    }
    this.startTime = newStartTime;
  }

  moveEndTimeBySteps(steps) {
    let newEndTime = math.add(this.endTime, math.multiply(steps, this.chart.step));

    if (!math.smaller(newEndTime, this.chart.closeTime)) {
      newEndTime = this.chart.closeTime;
    }

    if (!math.smaller(this.startTime, newEndTime)) {
      newEndTime = math.add(this.startTime, this.chart.step);
    }

    this.endTime = newEndTime;
  }

  covers(time) {
    const timeUnit = math.unit(time, 'h');
    return math.largerEq(timeUnit, this.startTime) && math.smaller(timeUnit, this.endTime);
  }

  clone() {
    return new Assignment(
      this.chart,
      this.id,
      this.employeeId,
      this.startTime.toNumber(),
      this.endTime.toNumber()
    );
  }
}
