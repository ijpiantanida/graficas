import superagent from 'superagent';
import M from 'front/helpers/messages';

export default class MessagesService {
  constructor(baseUrl) {
    this.baseUrl = baseUrl;
  }

  setLanguage(language) {
    return superagent.post(this.baseUrl + 'api/language/change')
      .send({language: language})
      .then((res) => {
        M.setMessages(res.body.language, res.body.messages);
      });
  }
}
