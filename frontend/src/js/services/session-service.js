import superagent from 'superagent';
import Cookies from 'front/helpers/cookies';

export default class SessionService {
  constructor(baseUrl) {
    this.baseUrl = baseUrl;
    this.cookieName = '_scheduler_session';
  }

  authenticate(username, password) {
    return superagent.post(this.baseUrl + 'api/session')
      .send({username: username, password: password})
      .then((res) => {
        this.createSessionCookie(res.body.session, res.body.expiresAt);
      }).catch((e) => {
        this.signOut();
        throw e;
      });
  }

  createSessionCookie(session, expiresAt) {
    const serializedSession = JSON.stringify({token: session, expiresAt: expiresAt});
    Cookies.set(this.cookieName, serializedSession);
  }

  validSession(req) {
    const session = this.getSession(req);
    if (!session) {
      return false;
    }
    return session.expiresAt > new Date();
  }

  getSession(req) {
    const savedValue = Cookies.get(this.cookieName, req);
    if (savedValue) {
      const parsedSession = JSON.parse(savedValue);
      parsedSession.expiresAt = new Date(parsedSession.expiresAt);
      return parsedSession;
    }
  }

  signOut() {
    Cookies.set(this.cookieName, '');
  }
}
