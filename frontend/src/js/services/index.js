import SessionService from 'front/services/session-service';
import MessagesService from 'front/services/messages-service';

const baseUrl = "/";

export default {
  session: new SessionService(baseUrl),
  language: new MessagesService(baseUrl)
};
