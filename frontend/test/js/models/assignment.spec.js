import Chart from 'front/models/chart';
import Assignment from 'front/models/assignment';

describe("Assignment", () => {
  const chart = new Chart(8, 22, 0.5);

  describe("#moveStartTimeBySteps", () => {
    it("sets the startTime to starTime plus the number of steps when adjusting positive and within range", () => {
      const assignment = new Assignment(chart, 1, 1, 8, 11);
      assignment.moveStartTimeBySteps(3);
      expect(assignment.startTime.toNumber()).to.equal(9.5);
    });

    it("sets the startTime to starTime minus the number of steps when adjusting negative and within range", () => {
      const assignment = new Assignment(chart, 1, 1, 20, 21);
      assignment.moveStartTimeBySteps(-10);
      expect(assignment.startTime.toNumber()).to.equal(15);
    });

    it("sets the startTime to the chart openTime if negative adjusting more steps than allowed", () => {
      const assignment = new Assignment(chart, 1, 1, 9, 21);
      assignment.moveStartTimeBySteps(-10);
      expect(assignment.startTime.toNumber()).to.equal(8);
    });

    it("sets the startTime to the endTime minus one step if positive adjusting more steps than allowed", () => {
      const assignment = new Assignment(chart, 1, 1, 19, 20);
      assignment.moveStartTimeBySteps(5);
      expect(assignment.startTime.toNumber()).to.equal(19.5);
    });
  });

  describe("#moveEndTimeBySteps", () => {
    it("sets the endTime to endTime plus the number of steps when adjusting positive and within range", () => {
      const assignment = new Assignment(chart, 1, 1, 11, 15);
      assignment.moveEndTimeBySteps(9);
      expect(assignment.endTime.toNumber()).to.equal(19.5);
    });

    it("sets the endTime to endTime minus the number of steps when adjusting negative and within range", () => {
      const assignment = new Assignment(chart, 1, 1, 11, 15);
      assignment.moveEndTimeBySteps(-1);
      expect(assignment.endTime.toNumber()).to.equal(14.5);
    });

    it("sets the endTime to the chart closeTime if positive adjusting more steps than allowed", () => {
      const assignment = new Assignment(chart, 1, 1, 11, 15);
      assignment.moveEndTimeBySteps(100);
      expect(assignment.endTime.toNumber()).to.equal(22);
    });

    it("sets the endTime to the startTime plus one step if negative adjusting more steps than allowed", () => {
      const assignment = new Assignment(chart, 1, 1, 11, 15);
      assignment.moveEndTimeBySteps(-100);
      expect(assignment.endTime.toNumber()).to.equal(11.5);
    });
  });

  describe("#clone", () => {
    it("returns an instance with the same properties", () => {
      const assignment = new Assignment(chart, 1, 1, 11, 15);
      const clone = assignment.clone();
      expect(clone.chart).to.equal(assignment.chart);
      expect(clone.id).to.equal(assignment.id);
      expect(clone.employeeId).to.deep.equal(assignment.employeeId);
      expect(clone.startTime).to.deep.equal(assignment.startTime);
      expect(clone.endTime).to.deep.equal(assignment.endTime);
    });
  });

  describe("#covers", () => {
    const assignment = new Assignment(chart, 1, 1, 11, 15);
    it("returns false when the time is before the assignment", () => {
      expect(assignment.covers(8)).to.equal(false);
    });

    it("returns false when the time is after the assignment", () => {
      expect(assignment.covers(20)).to.equal(false);
    });

    it("returns false when the time is right at the endTime", () => {
      expect(assignment.covers(15)).to.equal(false);
    });

    it("returns true when the time is right at the startTime", () => {
      expect(assignment.covers(11)).to.equal(true);
    });

    it("returns true when the time is in the middle of the assignment", () => {
      expect(assignment.covers(12)).to.equal(true);
    });
  });
});
