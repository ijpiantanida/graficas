import AssignmentSummary from 'front/models/assignmentSummary';
import Chart from 'front/models/chart';
import Assignment from 'front/models/assignment';

describe("AssignmentSummary", () => {
  describe("#numberOfAssignmentsByTimeSlot", () => {
    const chart = new Chart(8, 14, 1);
    chart.assignments = [
      new Assignment(chart, 1, 1, 8, 12),
      new Assignment(chart, 2, 2, 8, 9),
      new Assignment(chart, 3, 2, 10, 11),
      new Assignment(chart, 3, 3, 13, 14)
    ];

    it("returns an array of the sum of assignments by each time slot", () => {
      const summary = new AssignmentSummary(chart);
      expect(summary.numberOfAssignmentsByTimeSlot()).to.deep.equal([2, 1, 2, 1, 0, 1, 0]);
    });
  });
});
