import Chart from 'front/models/chart';
import Assignment from 'front/models/assignment';

describe("Chart", () => {
  describe("constructor", () => {
    it("starts with empty assignments", () => {
      const chart = new Chart(8, 9.5, 0.5);
      expect(chart.assignments).to.be.empty;
    });
  });

  describe("#forEachTimeSlot", () => {
    it("calls the callback with each time slot", () => {
      const chart = new Chart(8, 9.5, 0.5);
      const slots = [];
      chart.forEachTimeSlot((time) => {
        slots.push(time.toNumber());
      });

      expect(slots).to.deep.equal([8, 8.5, 9, 9.5]);
    });
  });

  describe("#assignmentsForEmployee", () => {
    it("returns the assignments for the employee", () => {
      const chart = new Chart(8, 9.5, 0.5);
      chart.assignments = [
        new Assignment(chart, 1, 1, 8, 9),
        new Assignment(chart, 2, 2, 8, 9),
        new Assignment(chart, 3, 1, 8, 9)
      ];

      expect(chart.assignmentsForEmployee({id: 1}).map((a) => a.id)).to.deep.equal([1,3]);
      expect(chart.assignmentsForEmployee({id: 2}).map((a) => a.id)).to.deep.equal([2]);
      expect(chart.assignmentsForEmployee({id: 3}).map((a) => a.id)).to.deep.equal([]);
    });
  });
});
