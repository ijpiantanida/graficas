import React from 'react';
import {mount} from 'enzyme';
import M, {t} from 'front/helpers/messages';

describe('Messages', () => {
  describe('component state update', () => {
    const Component = React.createClass({render: () => (<div>Hi</div>)});
    it("when setting messages it updates registered components state", () => {
      const c1 = mount(<Component />);
      const c2 = mount(<Component />);
      M.registerComponent(c1);
      M.registerComponent(c2);
      M.setMessages('en', {'foo.bar': 'A message'});
      expect(c1.state()["_MESSAGES_LANGUAGE"]).to.equal('en');
      expect(c2.state()["_MESSAGES_LANGUAGE"]).to.equal('en');
      M.unregisterComponent(c1);
      M.unregisterComponent(c2);
      c1.unmount();
      c2.unmount();
    });

    it("does nothing when trying to unregister an unknown component", () => {
      const c2 = mount(<Component />);
      M.unregisterComponent(c2);
      c2.unmount();
    });
  });

  describe('message resolution', () => {
    before(() => M.setMessages('en', {'foo.bar': 'A message'}));

    it('returns the message translation when the key exists', () => {
      expect(t('foo.bar')).to.eq('A message');
    });

    it("returns the defaultValue when the key doesn't exist", () => {
      expect(t('bar.notFoo', {}, 'Default value')).to.eq('Default value');
    });

    it("returns the key when the key doesn't exist and no default value was given", () => {
      expect(t('bar.notFoo')).to.eq('bar.notFoo');
    });
  });

  describe('interpolation', () => {
    before(() => M.setMessages('en', {'foo.bar': 'A message with {{firstParam}} and {{secondParam}}'}));
    it('it returns the string with the keys when no parameters are given', () => {
      expect(t('foo.bar')).to.eq('A message with {{firstParam}} and {{secondParam}}');
    });

    it('it returns the string with the interpolated values when the parameters are given', () => {
      expect(t('foo.bar', {firstParam: 'foo', secondParam: 'bar'})).to.eq('A message with foo and bar');
    });
  });
});
