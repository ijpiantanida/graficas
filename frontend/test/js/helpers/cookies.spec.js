import Cookies from 'front/helpers/cookies';

describe('Cookies', () => {
  describe('get', () => {
    describe("client", () => {
      it('gets a cookie from the document', () => {
        document.cookie = "SOME_COOKIE=Value; path=/";
        const value = Cookies.get('SOME_COOKIE');
        expect(value).to.eq('Value');
      });
    });

    describe("server", () => {
      it('gets a cookie from the request', () => {
        process.env.WEBPACK_ENV = "server";
        const req = {cookies: {test: 'Value'}};
        const value = Cookies.get('test', req);
        expect(value).to.eq('Value');
      });
    });
  });

  describe('set', () => {
    it('sets a cookie in the document', () => {
      Cookies.set('SOME_COOKIE', 'Value');
      expect(document.cookie).to.include('SOME_COOKIE=Value');
    });
  });
});
