import React from 'react';
import { shallow, mount } from 'enzyme';
import {t} from 'front/helpers/messages';

import HelloWorld from 'front/components/helloWorld';

describe("HelloWorld component", () => {
  it("shallow renders Hello World", () => {
    const params = {message: "Test"};
    const helloWorld = shallow(<HelloWorld params={params}/>);

    expect(helloWorld).to.have.text(t("hello_world.message", {message: "Test"}));
  });

  it("mounts renders Hello World", () => {
    const params = {message: "Test"};
    const helloWorld = mount(<HelloWorld params={params}/>);

    expect(helloWorld).to.have.text("Hello Test");
    helloWorld.unmount();
  });
});
