import React from 'react';
import AssignmentComponent from 'front/components/schedule-chart/assignment';
import ScheduleChart from 'front/components/schedule-chart';
import Assignment from 'front/models/assignment';
import Chart from 'front/models/chart';
import {shallow} from 'enzyme';

describe("Assignment component", () => {
  it("renders the assignment with the correct left and width", () => {
    const chart = new Chart(8, 20, 0.5);
    const assignment = new Assignment(chart, 1, 1, 9.5, 13);
    const element = shallow(<AssignmentComponent assignment={assignment}/>);

    expect(element).to.have.style('left', `${ScheduleChart.timeColumnWidth * 3}px`);
    expect(element).to.have.style('width', `${ScheduleChart.timeColumnWidth * 7}px`);
  });
});
