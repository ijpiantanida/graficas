import React from 'react';
import {shallow} from 'enzyme';
import AssignmentSummary from 'front/components/schedule-chart/assignmentSummary';

import Assignment from 'front/models/assignment';
import Chart from 'front/models/chart';

describe("AssignmentSummary component", () => {
  it("renders one value from the AssignmentSummary for each time slot", () => {
    const chart = new Chart(8, 10, 1);
    chart.assignments = [new Assignment(chart, 1, 1, 9, 10)];

    const summary = shallow(<AssignmentSummary chart={chart} />);
    const entries = summary.find('[data-bdd="assignment-summary-entry"]');
    expect(entries.length).to.equal(3);
    expect(entries.at(0).text()).to.equal("");
    expect(entries.at(1).text()).to.equal("1");
    expect(entries.at(2).text()).to.equal("");
  });
});
