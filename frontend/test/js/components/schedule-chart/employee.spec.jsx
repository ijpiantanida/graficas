import React from 'react';
import Employee from 'front/components/schedule-chart/employee';
import {shallow} from 'enzyme';

describe("Employee component", () => {
  it("renders the employee name", () => {
    const employee = {id: 1, name: "Employee 1"};
    const element = shallow(<Employee employee={employee} />);

    expect(element.text()).to.equal("Employee 1");
  });
});
