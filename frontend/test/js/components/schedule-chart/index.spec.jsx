import React from 'react';
import ScheduleChart from 'front/components/schedule-chart';
import Assignment from 'front/components/schedule-chart/assignment';
import AssignmentSummary from 'front/components/schedule-chart/assignmentSummary';
import Employee from 'front/components/schedule-chart/employee';
import HeaderTime from 'front/components/schedule-chart/header-time';
import {shallow} from 'enzyme';

describe("ScheduleChart component", () => {
  const scheduleChart = shallow(<ScheduleChart/>);
  it("renders a list of employees", () => {
    expect(scheduleChart.find(Employee).length).to.equal(3);
  });

  it("renders the time header", () => {
    const headerTime = scheduleChart.find(HeaderTime);
    expect(headerTime.length).to.equal(1);
  });

  it("renders all the assignments", () => {
    const assignments = scheduleChart.find(Assignment);
    expect(assignments.length).to.equal(4);
  });

  it("renders the AssignmentSummary", () => {
    const summary = scheduleChart.find(AssignmentSummary);
    expect(summary.length).to.equal(1);
  });
});
