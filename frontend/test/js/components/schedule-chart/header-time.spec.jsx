import React from 'react';
import HeaderTime from 'front/components/schedule-chart/header-time';
import Chart from 'front/models/chart';
import {shallow} from 'enzyme';

describe("HeaderTime component", () => {
  it("renders all the slots", () => {
    const chart = new Chart(9, 11, 0.5);

    const header = shallow(<HeaderTime chart={chart} />);
    const entries = header.find("[data-bdd='timetable-header-time']");
    expect(entries.map((e) => e.text())).to.deep.equal(["09:00", "09:30", "10:00", "10:30", "11:00"]);
  });
});
