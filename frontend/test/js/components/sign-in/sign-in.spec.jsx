import React from 'react';
import {shallow, mount} from 'enzyme';

import {browserHistory} from 'react-router';

import FakePromise from 'front-test/support/fake-promise';
import Services from 'front/services';

import SignIn from 'front/components/sign-in/sign-in';

describe("SignIn component", () => {
  afterEach(td.reset);

  context("mounting", () => {
    const signIn = mount(<SignIn />);
    signIn.unmount();
  });

  context("singing in", () => {
    it("shows a loading message instead of the submit button while doing auth", () => {
      const signIn = shallow(<SignIn />);

      const promise = new Promise(() => {
      });

      td.replace(Services.session, 'authenticate', () => {
        return promise;
      });

      signIn.find('form').simulate('submit', {
        preventDefault: () => {
        }
      });

      expect(signIn.find('button')).to.have.length(0);
      expect(signIn.find('[data-bdd="loading-message"]')).to.have.length(1);
    });

    context("when the credentials are valid", () => {
      beforeEach(() => {
        td.replace(Services.session, 'authenticate', () => {
          return FakePromise.resolve();
        });
        td.replace(browserHistory, 'push', td.function());
      });

      it("redirect to Home", () => {
        const signIn = shallow(<SignIn />);
        signIn.find('[data-bdd="username"]').simulate('change', {target: {value: 'john'}});
        signIn.find('[data-bdd="password"]').simulate('change', {target: {value: 'super-password'}});
        signIn.find('form').simulate('submit', {
          preventDefault: () => {
          }
        });

        td.verify(browserHistory.push('/'));
      });

      it("doesn't show back the sign-in button", () => {
        const signIn = shallow(<SignIn />);
        signIn.find('[data-bdd="username"]').simulate('change', {target: {value: 'john'}});
        signIn.find('[data-bdd="password"]').simulate('change', {target: {value: 'super-password'}});
        signIn.find('form').simulate('submit', {
          preventDefault: () => {
          }
        });

        expect(signIn.find('button')).to.have.length(0);
        expect(signIn.find('[data-bdd="loading-message"]')).to.have.length(1);
      });
    });

    context("when the credentials are not valid", () => {
      beforeEach(() => {
        td.replace(Services.session, 'authenticate', () => {
          return FakePromise.reject();
        });
      });

      it("shows an error message", () => {
        const signIn = shallow(<SignIn />);
        signIn.find('form').simulate('submit', {
          preventDefault: () => {
          }
        });

        expect(signIn.find('[data-bdd="error-message"]')).to.have.length(1);
      });

      it("shows back the sign-in button", () => {
        const signIn = shallow(<SignIn />);
        signIn.find('form').simulate('submit', {
          preventDefault: () => {
          }
        });

        expect(signIn.find('button')).to.have.length(1);
        expect(signIn.find('[data-bdd="loading-message"]')).to.have.length(0);
      });

      it("hides the error message when submitting again", () => {
        const signIn = shallow(<SignIn />);
        signIn.find('form').simulate('submit', {
          preventDefault: () => {
          }
        });

        const promise = new Promise(() => {
        });

        td.replace(Services.session, 'authenticate', () => {
          return promise;
        });

        signIn.find('form').simulate('submit', {
          preventDefault: () => {
          }
        });
        expect(signIn.find('[data-bdd="error-message"]')).to.have.length(0);
      });
    });
  });
});
