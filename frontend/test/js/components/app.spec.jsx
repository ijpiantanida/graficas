import React from 'react';
import { shallow, mount } from 'enzyme';
import {t} from 'front/helpers/messages';
import Services from 'front/services';
import {browserHistory} from 'react-router';

import App from 'front/components/app';

describe("App component", () => {
  it("renders main links", () => {
    const app = mount(<App><div>Test</div></App>);
    const links = app.find('Link');

    const firstLink = links.at(0);
    expect(firstLink.props().to).to.equal("/messages/about");
    expect(firstLink.html()).to.include(t("navigation.about"));
    const secondLink = links.at(1);
    expect(secondLink.props().to).to.equal("/messages/inbox");
    expect(secondLink.html()).to.include(t("navigation.inbox"));

    app.unmount();
  });

  it("renders its children", () => {
    const app = shallow(<App><div className="my-div">Test</div></App>);
    const div = app.find('.my-div');
    expect(div).to.have.length(1);
  });

  describe("Sign Out", () => {
    beforeEach(() => {
      Services.session.createSessionCookie("SESSION", new Date());
      td.replace(browserHistory, 'push', td.function());
    });

    const app = shallow(<App><div className="my-div">Test</div></App>);
    const signOut = app.find("[data-bdd='navigation-sign-out']");

    it("removes the session cookie", () => {
      signOut.simulate('click');
      const session = Services.session.getSession();
      expect(session).to.be.undefined;
    });

    it("redirects to the sign-in page", () => {
      signOut.simulate('click');
      td.verify(browserHistory.push('/sign-in'));
    });
  });

  describe("changing language", () => {
    it("calls the language service with the selected language", () => {
      td.replace(Services.language, 'setLanguage', td.function());
      const app = shallow(<App><div className="my-div">Test</div></App>);
      const language = app.find("[data-bdd='navigation-language-es']");
      language.simulate('click');
      td.verify(Services.language.setLanguage('es'));
    });
  });
});
