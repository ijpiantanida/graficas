class RejectedFakePromise {
    constructor(error) {
        this.error = error;
    }

    then() {
        return this;
    }

    catch(cb) {
        const value = cb(this.error);
        return new ResolvedFakePromise(value);
    }

    finally(cb) {
        try {
            const value = cb();
            return new ResolvedFakePromise(value);
        } catch (ex) {
            return new RejectedFakePromise(ex);
        }
    }
}

class ResolvedFakePromise {
    constructor(value) {
        this.value = value;
    }

    then(cb) {
        try {
            const newValue = cb(this.value);
            return new ResolvedFakePromise(newValue);
        } catch (ex) {
            return new RejectedFakePromise(ex);
        }
    }

    catch() {
        return this;
    }

    finally(cb) {
        try {
            const newValue = cb(value);
            return new ResolvedFakePromise(newValue);
        } catch (ex) {
            return new RejectedFakePromise(ex);
        }
    }
}

export default class {
    static resolve(value) {
        return new ResolvedFakePromise(value);
    }

    static reject(error) {
        return new RejectedFakePromise(error);
    }
};
