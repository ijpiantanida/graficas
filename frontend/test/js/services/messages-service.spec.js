import MessagesService from 'front/services/messages-service';
import superagent from 'superagent';
const superagentMocker = require('superagent-mocker')(superagent);
import M, {t} from 'front/helpers/messages';

const service = new MessagesService("http://my-service.com/");
describe("MessagesService", () => {
  beforeEach(() => {
    M.setMessages('en', {"default.keys": "Value 1"});
  });
  describe("#setLanguage", () => {
    context("when the language change is successful", () => {
      it("changes the messages", () => {
        superagentMocker.post('http://my-service.com/api/language/change', (req) => {
          expect(req.body.language).to.equal('es');
          return {
            body: {messages: {"foo.bar": "Valor 1"}}
          };
        });

        return service.setLanguage('es').then(() => {
          expect(t("foo.bar")).to.equal("Valor 1");
        });
      });
    });

    context("when the authentication is unsuccessful", () => {
      it("clears the session", (done) => {
        superagentMocker.post('http://my-service.com/api/language/change', (req) => {
          return {
            status: 401,
            body: {}
          };
        });

        service.setLanguage('es').then(() => {
          done(new Error("Promise shouldn't have been resolved"));
        }).catch(() => {
          expect(t("default.keys")).to.equal("Value 1");
          done();
        });
      });
    });
  });
});
