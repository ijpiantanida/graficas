import SessionService from 'front/services/session-service';
import superagent from 'superagent';
const superagentMocker = require('superagent-mocker')(superagent);
import tk from 'timekeeper';
import Cookies from 'front/helpers/cookies';

const service = new SessionService("http://my-service.com/");
describe("SessionService", () => {
  const mockedSession = {token: "A SESSION", expiresAt: new Date()};
  beforeEach(() => {
    const getFn = td.function();
    td.when(getFn(service.cookieName, undefined)).thenReturn(JSON.stringify(mockedSession));

    td.replace(Cookies, 'set', td.function());
    td.replace(Cookies, 'get', getFn);
  });

  afterEach(tk.reset);
  afterEach(td.reset);
  afterEach(service.signOut);

  describe("#authenticate", () => {
    context("when the authentication is successful", () => {
      it("saves the session", () => {
        const expiresAt = new Date();
        superagentMocker.post('http://my-service.com/api/session', (req) => {
          expect(req.body.username).to.equal('john');
          expect(req.body.password).to.equal('super-password');
          return {
            body: {session: "A SESSION", expiresAt: expiresAt}
          };
        });

        return service.authenticate('john', 'super-password').then(() => {
          td.verify(Cookies.set(service.cookieName, JSON.stringify({token: "A SESSION", expiresAt: expiresAt})));
        });
      });
    });

    context("when the authentication is unsuccessful", () => {
      it("clears the session", (done) => {
        superagentMocker.post('http://my-service.com/api/session', (req) => {
          return {
            status: 401,
            body: {}
          };
        });

        service.authenticate('john', 'super-password').then(() => {
          done(new Error("Promise shouldn't have been resolved"));
        }).catch(() => {
          td.verify(Cookies.set(service.cookieName, ''));
          done();
        });
      });
    });
  });

  describe("#validSession", () => {
    it("returns true when the session is valid", () => {
      const newSession = {token: "A SESSION", expiresAt: new Date(2015, 3, 1)};
      const getFn = td.function();
      td.when(getFn(service.cookieName, undefined)).thenReturn(JSON.stringify(newSession));
      td.replace(Cookies, 'get', getFn);

      tk.travel(new Date(2010, 3, 2));

      expect(service.validSession()).to.equal(true);
    });

    it("returns false when there is no session", () => {
      const getFn = td.function();
      td.when(getFn(service.cookieName, undefined)).thenReturn(undefined);
      td.replace(Cookies, 'get', getFn);

      expect(service.validSession()).to.equal(false);
    });

    it("returns false when the session is expired", () => {
      const newSession = {token: "A SESSION", expiresAt: new Date(2011, 6, 4)};
      const getFn = td.function();
      td.when(getFn(service.cookieName, undefined)).thenReturn(JSON.stringify(newSession));
      td.replace(Cookies, 'get', getFn);

      tk.travel(new Date(2015, 3, 2));

      expect(service.validSession()).to.equal(false);
    });
  });

  describe("#getSession", () => {
    it("returns the session from the cookie", () => {
      const session = service.getSession();
      expect(session.token).to.equal(mockedSession.token);
      expect(session.expiresAt).to.equalTime(mockedSession.expiresAt);
    });
  });

  describe("#signOut", () => {
    it("removes the session from the cookie", () => {
      service.signOut();
      td.verify(Cookies.set(service.cookieName, ''));
    });
  });
});
