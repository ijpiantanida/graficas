// Setup jsdom
import {jsdom} from 'jsdom';

const exposedProperties = ['window', 'navigator', 'document'];

global.document = jsdom('');
global.window = document.defaultView;
Object.keys(document.defaultView).forEach((property) => {
  if (typeof global[property] === 'undefined') {
    exposedProperties.push(property);
    global[property] = document.defaultView[property];
  }
});

global.navigator = {
  userAgent: 'node.js'
};

import 'ignore-styles';

//Setup chai
import chai, {expect} from 'chai';
chai.use(require('chai-datetime'));
chai.use(require('chai-enzyme')());
global.expect = expect;

global.td = require('testdouble');

import Messages from 'server/resources/messages/en.json';
import M from 'front/helpers/messages';
M.setMessages('en', Messages);
