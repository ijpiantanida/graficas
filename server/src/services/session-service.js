import Session from 'server/models/session';
import Promise from 'bluebird';

module.exports = class SessionService {
  static sessionValidityMinutes() {
    return 300;
  }

  authenticate(username, password) {
    return new Promise((resolve, reject) => {
      if (username === "username" && password === "password") {
        const expireDate = new Date();
        expireDate.setMinutes(expireDate.getMinutes() + SessionService.sessionValidityMinutes());
        const session = new Session(username, expireDate);
        resolve(session);
      } else {
        reject();
      }
    });
  }
};
