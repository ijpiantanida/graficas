module.exports = class MessagesService {
  constructor() {
    this.messagesByLanguage = {};
  }

  register(language, messages) {
    this.messagesByLanguage[language] = messages;
  }

  forLanguage(language) {
    return this.messagesByLanguage[language];
  }
};
