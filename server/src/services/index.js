import SessionService from 'server/services/session-service';
import MessagesService from 'server/services/messages-service';

import EnMessages from 'server/resources/messages/en.json';
import EsMessages from 'server/resources/messages/es.json';

const messagesService = new MessagesService();
messagesService.register("en", EnMessages);
messagesService.register("es", EsMessages);

module.exports = {
  session: new SessionService(),
  messages: messagesService
};
