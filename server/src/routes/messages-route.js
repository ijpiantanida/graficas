import Services from 'server/services';

module.exports = {
  setup(app, config) {
    app.post('/api/language/change', (req, res) => {
      const language = req.body.language;
      const messages = Services.messages.forLanguage(language);
      if (messages) {
        res.cookie('LANGUAGE', language);
        res.status(200).send({messages: messages, language: language});
      } else {
        res.status(400).end();
      }
    });
  }
};
