import SessionRoutes from 'server/routes/session-route';
import MessagesRoutes from 'server/routes/messages-route';

import Services from 'server/services';
import M from 'front/helpers/messages';

import Routes from 'front/router';

class ReactApp {
  constructor(config) {
    this.config = config;
  }

  handle(req, res) {
    const language = req.cookies["LANGUAGE"] || 'en';
    const messages = Services.messages.forLanguage(language);
    M.setMessages(language, messages);
    const messagesJSON = {messages: messages, language: language};

    // match({routes: Routes.setup(req), location: req.url}, (error, redirectLocation, renderProps) => {
    //   if (error) {
    //     res.status(500).send(error.message);
    //   } else if (redirectLocation) {
    //     res.redirect(302, redirectLocation.pathname + redirectLocation.search);
    //   } else if (renderProps) {
    //     const reactApp = ReactDOM.renderToString(<RouterContext {...renderProps} />);
    res.render('home', {
      messages: JSON.stringify(messagesJSON),
      config: this.config,
      react: ''
    });
    //   } else {
    //     res.status(404).render('not-found');
    //   }
    // });
  }
}

module.exports = {
  setup(app, config) {
    const reactApp = new ReactApp(config);

    SessionRoutes.setup(app, config);
    MessagesRoutes.setup(app, config);

    app.get('/', (req, res) => {
      reactApp.handle(req, res);
    });

    app.get('*', (req, res) => {
      reactApp.handle(req, res);
    });
  }
};
