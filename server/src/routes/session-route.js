import Services from 'server/services';

module.exports = {
  setup(app, config) {
    app.post('/api/session', (req, res) => {
      const sessionPromise = Services.session.authenticate(req.body.username, req.body.password);
      sessionPromise.then((session) => {
        const crypto = session.toCrypto();
        res.status(200).send({session: crypto, expiresAt: session.expiresAt});
      }).catch(() => {
        res.status(401).end();
      });
    });
  }
};
