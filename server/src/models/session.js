import nodeCryptoJsAes from 'node-cryptojs-aes';
const CryptoJS = nodeCryptoJsAes.CryptoJS;

module.exports = class Session {
  constructor(userId, expiresAt) {
    this.userId = userId;
    this.expiresAt = expiresAt;
  }

  static setPassphrase(passphrase) {
    this.passphrase = passphrase;
  }

  static fromCrypto(crypto, onInvalid) {
    try {
      const decryptedSession = CryptoJS.AES.decrypt(crypto, Session.passphrase.toString("base64"));
      const jsonSession = CryptoJS.enc.Utf8.stringify(decryptedSession);
      const parsedSession = JSON.parse(jsonSession);

      const expiresAt = new Date(parsedSession.expiresAt);
      return new Session(parsedSession.userId, expiresAt);
    } catch (e) {
      onInvalid();
      return;
    }
  }

  isValid() {
    return new Date() < this.expiresAt;
  }

  toCrypto() {
    const json = JSON.stringify({userId: this.userId, expiresAt: this.expiresAt.getTime()});
    const encrypted = CryptoJS.AES.encrypt(json, Session.passphrase.toString("base64"));
    return encrypted.toString();
  }
};
