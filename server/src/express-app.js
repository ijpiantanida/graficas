const express = require('express');
const exphbs = require('express-handlebars');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');

const Session = require('./models/session');
Session.setPassphrase("VERY BIZARRE PASSPHRASE");

const Routes = require('./routes/routes');

const buildPath = __dirname + '/../../build';

const app = express();

app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.engine('.hbs', exphbs({extname: '.hbs'}));
app.set('view engine', '.hbs');
app.set('views', buildPath + '/templates/');

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use(cookieParser());

if (process.env.NODE_ENV !== "test") {
  app.use(morgan('dev')); // Logs
}

if (process.env.NODE_ENV !== "production" && process.env.NODE_ENV !== "test") {
  const webpack = require('webpack');
  const webpackConfig = require(__dirname + '/../../webpack.config.js').devConfigClient;
  const compiler = webpack(webpackConfig);

  app.use(require("webpack-dev-middleware")(compiler, {
    noInfo: true, publicPath: webpackConfig.output.publicPath
  }));

  app.use(require("webpack-hot-middleware")(compiler));
}

app.use('/static', express.static(buildPath + '/static/'));

const config = {
  cssLink: process.env.NODE_ENV === "production"
};

Routes.setup(app, config);

module.exports = app;
