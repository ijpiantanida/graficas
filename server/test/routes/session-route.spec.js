import request from 'supertest';
import app from 'server/express-app';

describe("Session Routes", () => {
  describe("POST /api/session", () => {
    context("when the authentication data is valid", () => {
      it("returns the session", () => {
        return request(app)
          .post('/api/session')
          .send({username: 'username', password: 'password'})
          .expect(200)
          .expect((res) => {
            expect(res.body.session).to.not.be.undefined;
            expect(res.body.expiresAt).to.not.be.undefined;
          });
      });
    });

    context("when the authentication data is not valid", () => {
      it("returns a 401", () => {
        return request(app)
          .post('/api/session')
          .send({username: 'NOT', password: 'password'})
          .expect(401, '');
      });
    });
  });
});
