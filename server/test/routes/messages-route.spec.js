import request from 'supertest';
import app from 'server/express-app';
import Services from 'server/services';

describe("Messages Routes", () => {
  describe("POST /api/language/change", () => {
    context("when the language is valid", () => {
      it("returns the new messages", () => {
        return request(app)
          .post('/api/language/change')
          .send({language: 'en'})
          .expect(200)
          .expect((res) => {
            expect(res.headers['set-cookie']).to.deep.equal([ 'LANGUAGE=en; Path=/' ]);
            expect(res.body.language).to.equal('en');
            expect(res.body.messages).to.deep.equal(Services.messages.forLanguage('en'));
          });
      });
    });

    context("when the language is not valid", () => {
      it("returns a 400", () => {
        return request(app)
          .post('/api/language/change')
          .send({language: 'INVALID'})
          .expect(400, '');
      });
    });
  });
});
