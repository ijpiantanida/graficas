import MessagesService from 'server/services/messages-service';

describe("MessagesService", () => {
  describe("#forLanguage", () => {
    let service;
    beforeEach(() => {
      service = new MessagesService();
      service.register("en", {"key_1": "Value 1"});
      service.register("es", {"key_1": "Valor 1"});
    });

    it("returns the messages when the language exists", () => {
      expect(service.forLanguage("en")).to.deep.equal({"key_1": "Value 1"});
      expect(service.forLanguage("es")).to.deep.equal({"key_1": "Valor 1"});
    });

    it("returns undefined when the language doesn't exist", () => {
      expect(service.forLanguage("INVALID")).to.be.undefined;
    });
  });
});
