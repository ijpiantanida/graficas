import SessionService from 'server/services/session-service';
import tk from 'timekeeper';

describe("SessionService", () => {
  afterEach(tk.reset);

  describe("#authenticate", () => {
    context("when the authentication is correct", () => {
      it("returns a session with 30 minutes validity", () => {
        const now = new Date(2016, 3, 1, 12, 23);
        tk.freeze(now);
        const expectedExpire = new Date(now);
        expectedExpire.setMinutes(now.getMinutes() + SessionService.sessionValidityMinutes());

        const sessionPromise = new SessionService().authenticate("username", "password");

        return sessionPromise.then((session) => {
          expect(session.userId).to.equal("username");
          expect(session.expiresAt).to.equalTime(expectedExpire);
        });
      });
    });

    context("when the authentication is not correct", () => {
      it("runs the onInvalid callback when the password is invalid", (done) => {
        const sessionPromise = new SessionService().authenticate("username", "NOT VALID");
        sessionPromise.then(() => {
          done(new Error("The promise shouldn't have been resolved"));
        }).catch(() => {
          done();
        });
      });

      it("runs the onInvalid callback when the username is invalid", (done) => {
        const sessionPromise = new SessionService().authenticate("NOT VALID", "password");
        sessionPromise.then(() => {
          done(new Error("The promise shouldn't have been resolved"));
        }).catch(() => {
          done();
        });
      });
    });
  });
});
