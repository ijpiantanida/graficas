import Session from 'server/models/session';
import tk from 'timekeeper';

describe("Session", () => {
  const userId = 123;
  const expiresAt = new Date(2015, 10, 21, 8, 44);
  const session = new Session(userId, expiresAt);

  before(() => {
    Session.setPassphrase("TEST");
  });
  afterEach(tk.reset);

  it("initializes the session", () => {
    expect(session.userId).to.equal(userId);
    expect(session.expiresAt).to.equalTime(expiresAt);
  });

  describe("#isValid", () => {
    it("returns false when now is after expire date", () => {
      const session = new Session(userId, expiresAt);

      tk.travel(new Date(2015, 10, 21, 9));
      expect(session.isValid()).to.equal(false);
    });

    it("returns true when now is before expire date", () => {
      const session = new Session(userId, expiresAt);

      tk.travel(new Date(2015, 10, 21, 4));
      expect(session.isValid()).to.equal(true);
    });
  });

  describe(".fromCrypto", () => {
    context("when the hash is valid", () => {
      it("creates the session", () => {
        const hash = session.toCrypto();
        const newSession = Session.fromCrypto(hash, () => {});
        expect(newSession.userId).to.equal(userId);
        expect(newSession.expiresAt).to.equalTime(expiresAt);
      });

      it("doesn't runs the onInvalid function", () => {
        let val = 0;
        const hash = session.toCrypto();
        Session.fromCrypto(hash, () => {});
        expect(val).to.equal(0);
      });
    });

    context("when the hash is not valid", () => {
      it("runs the onInvalid function", () => {
        let val = 0;
        Session.fromCrypto("WHATEVER", () => {
          val = 5;
        });
        expect(val).to.equal(5);
      });
    });
  });
});
