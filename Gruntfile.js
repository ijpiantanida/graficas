var webpackConfig = require("./webpack.config.js");

module.exports = function (grunt) {
  grunt.initConfig({
    webpack: {
      "build-client-prod": webpackConfig.prodConfig,
      "build-client-dev": webpackConfig.devConfigClient
    },
    watch: {
      "sass": {
        files: ["frontend/src/sass/**/*"],
        tasks: ["webpack:build-client-dev"],
        options: {
          spawn: false,
          interrupt: true
        }
      },
      "templates": {
        files: ["frontend/src/templates/**/*"],
        tasks: ["copy"],
        options: {
          spawn: false,
          interrupt: true
        }
      }
    },
    copy: {
      templates: {
        expand: true,
        cwd: 'frontend/src/templates',
        src: '**',
        dest: 'build/templates',
      },
      img: {
        expand: true,
        cwd: 'frontend/src/img',
        src: '**',
        dest: 'build/static/img',
      },
    },
    clean: [__dirname + '/build']

  });

  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-webpack');

  grunt.registerTask("build", ["clean", "copy", "webpack:build-client-prod"]);
  grunt.registerTask("default", ["build"]);
  grunt.registerTask("dev", ["clean", "copy", "webpack:build-server", "watch"]);
};
