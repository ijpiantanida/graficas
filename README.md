#React Boilerplate

The project includes:
 - Babel
 - Grunt
 - React
 - React-Router
 - Webpack
 - Webpack-dev-server with HMR
 - Sass webpack loader
 - Mocha
 - Chai
 - Enzyme (and chai-enzyme)
 - jsdom

 ## How to use
 # Development
 * **npm run server** will start the server in development mode.
 * **grunt webpack-dev-server** will start the dev-server that hot reloads webpack's modules. All unkown requests will be proxied to the app.

 # Production
 * **grunt build** will build all the assets in the build folder
 * **NODE_ENV=production npm run server** will start the server in production mode, which will use the static compiled assets.